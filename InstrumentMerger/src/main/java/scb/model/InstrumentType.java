package scb.model;

/**
 * Created by shutao on 9/1/18.
 */
public enum InstrumentType {
    LME, PRIME
}
