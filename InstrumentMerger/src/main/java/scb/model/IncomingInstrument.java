package scb.model;

import java.time.ZonedDateTime;

/**
 * Created by shutao on 9/1/18.
 */
public class IncomingInstrument {
    protected InstrumentType instrumentType;
    protected String code;
    protected ZonedDateTime lastTradingDate;
    protected ZonedDateTime deliveryDate;
    protected String market;
    protected String label;
    protected String exchangeCode;
    protected boolean tradable;

    // Use lombok annotation will be a better choice. But it is a 3rd party lib.

    public InstrumentType getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(InstrumentType instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ZonedDateTime getLastTradingDate() {
        return lastTradingDate;
    }

    public void setLastTradingDate(ZonedDateTime lastTradingDate) {
        this.lastTradingDate = lastTradingDate;
    }

    public ZonedDateTime getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(ZonedDateTime deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getExchangeCode() {
        return exchangeCode;
    }

    public void setExchangeCode(String exchangeCode) {
        this.exchangeCode = exchangeCode;
    }

    public boolean isTradable() {
        return tradable;
    }

    public void setTradable(boolean tradable) {
        this.tradable = tradable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncomingInstrument that = (IncomingInstrument) o;

        if (tradable != that.tradable) return false;
        if (instrumentType != that.instrumentType) return false;
        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (lastTradingDate != null ? !lastTradingDate.equals(that.lastTradingDate) : that.lastTradingDate != null)
            return false;
        if (deliveryDate != null ? !deliveryDate.equals(that.deliveryDate) : that.deliveryDate != null) return false;
        if (market != null ? !market.equals(that.market) : that.market != null) return false;
        if (label != null ? !label.equals(that.label) : that.label != null) return false;
        return exchangeCode != null ? exchangeCode.equals(that.exchangeCode) : that.exchangeCode == null;
    }

    @Override
    public int hashCode() {
        int result = instrumentType != null ? instrumentType.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (lastTradingDate != null ? lastTradingDate.hashCode() : 0);
        result = 31 * result + (deliveryDate != null ? deliveryDate.hashCode() : 0);
        result = 31 * result + (market != null ? market.hashCode() : 0);
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + (exchangeCode != null ? exchangeCode.hashCode() : 0);
        result = 31 * result + (tradable ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "IncomingInstrument{" +
                "instrumentType=" + instrumentType +
                ", code='" + code + '\'' +
                ", lastTradingDate=" + lastTradingDate +
                ", deliveryDate=" + deliveryDate +
                ", market='" + market + '\'' +
                ", label='" + label + '\'' +
                ", exchangeCode='" + exchangeCode + '\'' +
                ", tradable=" + tradable +
                '}';
    }
}
