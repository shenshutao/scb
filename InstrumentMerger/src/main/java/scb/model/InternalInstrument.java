package scb.model;

import java.time.ZonedDateTime;

/**
 * Created by shutao on 9/1/18.
 */
public class InternalInstrument {

    protected ZonedDateTime lastTradingDate;
    protected ZonedDateTime deliveryDate;
    protected String market;
    protected String label;
    protected boolean tradable;

    public ZonedDateTime getLastTradingDate() {
        return lastTradingDate;
    }

    public void setLastTradingDate(ZonedDateTime lastTradingDate) {
        this.lastTradingDate = lastTradingDate;
    }

    public ZonedDateTime getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(ZonedDateTime deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isTradable() {
        return tradable;
    }

    public void setTradable(boolean tradable) {
        this.tradable = tradable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InternalInstrument that = (InternalInstrument) o;

        if (tradable != that.tradable) return false;
        if (lastTradingDate != null ? !lastTradingDate.equals(that.lastTradingDate) : that.lastTradingDate != null)
            return false;
        if (deliveryDate != null ? !deliveryDate.equals(that.deliveryDate) : that.deliveryDate != null) return false;
        if (market != null ? !market.equals(that.market) : that.market != null) return false;
        return label != null ? label.equals(that.label) : that.label == null;
    }

    @Override
    public int hashCode() {
        int result = lastTradingDate != null ? lastTradingDate.hashCode() : 0;
        result = 31 * result + (deliveryDate != null ? deliveryDate.hashCode() : 0);
        result = 31 * result + (market != null ? market.hashCode() : 0);
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + (tradable ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "| LAST_TRADING_DATE | DELIVERY_DATE | MARKET | LABEL | TRADABLE |" + System.lineSeparator() +
                "| " + lastTradingDate + " | " + deliveryDate + " | " + market + " | " + label + " | " + tradable + " |";
    }
}
