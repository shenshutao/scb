package scb.db;

import scb.model.InternalInstrument;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by shutao on 13/1/18.
 * Simulate the database.
 */
public class InternalInstrumentDatabase {
    private static Map<String, InternalInstrument> internalInstrumentMap = new ConcurrentHashMap<>();

    public InternalInstrument getInternalInstrument(String code) {
        return internalInstrumentMap.get(code);
    }

    public boolean saveOrUpdateInternalInstrument(String code, InternalInstrument internalInstrument) {
        InternalInstrumentDatabase.internalInstrumentMap.put(code, internalInstrument);
        return true;
    }
}
