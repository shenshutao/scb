package scb.service;

import scb.model.InstrumentType;
import scb.service.impl.MergerLME;
import scb.service.impl.MergerPRIME;

/**
 * Created by shutao on 10/1/18.
 */
public class InstrumentMergerFactory {
    public static AbstractMerger getMerger(InstrumentType instrumentType) {
        if (InstrumentType.LME.equals(instrumentType)) {
            return new MergerLME();
        } else if (InstrumentType.PRIME.equals(instrumentType)) {
            return new MergerPRIME();
        }

        return null;
    }
}
