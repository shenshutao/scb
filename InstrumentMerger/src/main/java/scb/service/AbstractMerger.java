package scb.service;

import scb.model.IncomingInstrument;
import scb.model.InternalInstrument;

/**
 * Created by shutao on 8/1/18.
 */
public abstract class AbstractMerger {
    public abstract InternalInstrument mergeInstrument(InternalInstrument internal, IncomingInstrument incoming);


    protected String getTargetMarket(String passInMarket) {

        if (passInMarket != null && passInMarket.matches("LME_.*")) {
            return passInMarket.substring(4);
        }
        return passInMarket;
    }
}
