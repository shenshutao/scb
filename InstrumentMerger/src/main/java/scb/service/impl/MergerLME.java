package scb.service.impl;

import scb.model.IncomingInstrument;
import scb.model.InternalInstrument;
import scb.service.AbstractMerger;


/**
 * Created by shutao on 10/1/18.
 */
public class MergerLME extends AbstractMerger {

    @Override
    public InternalInstrument mergeInstrument(InternalInstrument internal, IncomingInstrument incoming) {
        if (internal == null) {
            internal = new InternalInstrument();
        }
        internal.setLastTradingDate(incoming.getLastTradingDate());
        internal.setDeliveryDate(incoming.getDeliveryDate());
        internal.setMarket(getTargetMarket(incoming.getMarket()));
        internal.setLabel(incoming.getLabel());
        internal.setTradable(true);

        return internal;
    }
}
