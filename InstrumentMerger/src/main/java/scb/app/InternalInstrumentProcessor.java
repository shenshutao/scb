package scb.app;

import scb.db.InternalInstrumentDatabase;
import scb.model.IncomingInstrument;
import scb.model.InstrumentType;
import scb.model.InternalInstrument;
import scb.service.AbstractMerger;
import scb.service.InstrumentMergerFactory;

/**
 * Created by shutao on 9/1/18.
 */
public class InternalInstrumentProcessor {
    private InternalInstrumentDatabase internalInstrumentDatabase;

    public void setInternalInstrumentDatabase(InternalInstrumentDatabase internalInstrumentDatabase) {
        this.internalInstrumentDatabase = internalInstrumentDatabase;
    }

    public boolean publishInstrument(IncomingInstrument incomingInstrument) {
        return this.publishInstrument(incomingInstrument, null);
    }

    public boolean publishInstrument(IncomingInstrument incomingInstrument, String alternateCode) {
        InstrumentType type = incomingInstrument.getInstrumentType();

        // Logic to get the target instrument code.
        String code = null;
        if (alternateCode != null) {
            // If pass in alternate code, use it as first priority.
            code = alternateCode;
        } else if (InstrumentType.LME.equals(type)) {
            // If it is a LME instrument, use the code.
            code = incomingInstrument.getCode();
        } else {
            // Else use exchange code as default.
            code = incomingInstrument.getExchangeCode();
        }

        if (code == null) {
            return false;
        }

        /*
         * Atomic operation, 1.get instrument, 2.merge instrument, 3.store instrument.
         * For concurrent API call.
         * Currently is whole map level lock.
         */
        synchronized (InternalInstrumentProcessor.class) {
            // Get the target internal instrument.
            InternalInstrument targetInternalInstrument = internalInstrumentDatabase.getInternalInstrument(code);

            // Get Instrument Merger.
            AbstractMerger merger = InstrumentMergerFactory.getMerger(incomingInstrument.getInstrumentType());
            InternalInstrument mergedInstrument = merger.mergeInstrument(targetInternalInstrument, incomingInstrument);

            // update internal instrument.
            internalInstrumentDatabase.saveOrUpdateInternalInstrument(code, mergedInstrument);
        }
        return true;
    }
}
