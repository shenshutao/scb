package scb.app;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import scb.db.InternalInstrumentDatabase;
import scb.model.IncomingInstrument;
import scb.model.InstrumentType;
import scb.model.InternalInstrument;
import scb.service.AbstractMerger;
import scb.service.InstrumentMergerFactory;
import scb.util.BasicTestComponents;

import java.util.logging.Logger;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

/**
 * Created by shutao on 9/1/18.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({InternalInstrumentDatabase.class,
        InstrumentMergerFactory.class})
public class InternalInstrumentProcessorTest extends BasicTestComponents {
    Logger log = Logger.getLogger("demoLog");

    private InternalInstrumentProcessor internalInstrumentProcessor;
    private IncomingInstrument incomingInstrumentLME;
    private IncomingInstrument incomingInstrumentPRIME;
    private InternalInstrument internalInstrument;
    private InternalInstrumentDatabase mockInternalInstrumentDatabase;
    private AbstractMerger mockAbstractMerger;

    @Before
    public void init() {
        incomingInstrumentLME = getIncomingInstrumentLME();
        internalInstrument = getInternalInstrument();
        incomingInstrumentPRIME = getIncomingInstrumentPRIME();

        mockInternalInstrumentDatabase = PowerMockito.mock(InternalInstrumentDatabase.class);
        PowerMockito.mockStatic(InstrumentMergerFactory.class);
        mockAbstractMerger = PowerMockito.mock(AbstractMerger.class);
        PowerMockito.when(mockInternalInstrumentDatabase.getInternalInstrument(any())).thenReturn(null);
        PowerMockito.when(mockInternalInstrumentDatabase.saveOrUpdateInternalInstrument(any(), anyObject())).thenReturn(true);
        PowerMockito.when(mockAbstractMerger.mergeInstrument(anyObject(), anyObject())).thenReturn(internalInstrument);

        // Init processor
        internalInstrumentProcessor = new InternalInstrumentProcessor();
        internalInstrumentProcessor.setInternalInstrumentDatabase(mockInternalInstrumentDatabase);
    }

    @Test
    public void testPublishLMEInstrument() throws Exception {
        PowerMockito.when(InstrumentMergerFactory.getMerger(eq(InstrumentType.LME))).thenReturn(mockAbstractMerger);

        // Public Instrument
        internalInstrumentProcessor.publishInstrument(incomingInstrumentLME);

        verify(mockInternalInstrumentDatabase).saveOrUpdateInternalInstrument(eq("PB_03_2018"), eq(internalInstrument));
        verify(mockAbstractMerger).mergeInstrument(eq(null), eq(incomingInstrumentLME));
        verifyStatic(InstrumentMergerFactory.class, times(1));
    }

    @Test
    public void testPublishPRIMEInstrument() throws Exception {
        PowerMockito.when(InstrumentMergerFactory.getMerger(eq(InstrumentType.PRIME))).thenReturn(mockAbstractMerger);

        // Public Instrument
        incomingInstrumentPRIME.setExchangeCode("PB_05_2018");
        internalInstrumentProcessor.publishInstrument(incomingInstrumentPRIME);

        verify(mockInternalInstrumentDatabase).saveOrUpdateInternalInstrument(eq("PB_05_2018"), eq(internalInstrument));
        verify(mockAbstractMerger).mergeInstrument(eq(null), eq(incomingInstrumentPRIME));
        verifyStatic(InstrumentMergerFactory.class, times(1));
    }

    @Test
    public void testPublishInstrumentWithCustomizedCode() throws Exception {
        PowerMockito.when(InstrumentMergerFactory.getMerger(eq(InstrumentType.LME))).thenReturn(mockAbstractMerger);

        // Public Instrument
        internalInstrumentProcessor.publishInstrument(incomingInstrumentLME, "PB_04_2018");

        verify(mockInternalInstrumentDatabase).saveOrUpdateInternalInstrument(eq("PB_04_2018"), eq(internalInstrument));
        verify(mockAbstractMerger).mergeInstrument(eq(null), eq(incomingInstrumentLME));
        verifyStatic(InstrumentMergerFactory.class, times(1));
    }
}
