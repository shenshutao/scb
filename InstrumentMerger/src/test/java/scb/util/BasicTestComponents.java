package scb.util;

import scb.model.IncomingInstrument;
import scb.model.InstrumentType;
import scb.model.InternalInstrument;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Created by shutao on 14/1/18.
 */
public class BasicTestComponents {

    public static InternalInstrument getInternalInstrument() {
        InternalInstrument internalInstrument = new InternalInstrument();
        internalInstrument.setLastTradingDate(ZonedDateTime.of(LocalDateTime.of(2018, 5, 15, 0, 0), ZoneId.of("Europe/London")));
        internalInstrument.setDeliveryDate(ZonedDateTime.of(LocalDateTime.of(2018, 5, 15, 0, 0), ZoneId.of("Europe/London")));
        internalInstrument.setMarket("PB");
        internalInstrument.setLabel("Lead 13 March 2018");
        internalInstrument.setTradable(true);
        return internalInstrument;
    }

    public IncomingInstrument getIncomingInstrumentLME() {
        IncomingInstrument incomingInstrumentLME = new IncomingInstrument();
        incomingInstrumentLME.setCode("PB_03_2018");
        incomingInstrumentLME.setInstrumentType(InstrumentType.LME);
        incomingInstrumentLME.setLastTradingDate(ZonedDateTime.of(LocalDateTime.of(2018, 5, 15, 0, 0), ZoneId.of("Europe/London")));
        incomingInstrumentLME.setDeliveryDate(ZonedDateTime.of(LocalDateTime.of(2018, 5, 15, 0, 0), ZoneId.of("Europe/London")));
        incomingInstrumentLME.setMarket("LME_PB");
        incomingInstrumentLME.setLabel("Lead 13 March 2018");
        return incomingInstrumentLME;
    }

    public IncomingInstrument getIncomingInstrumentPRIME() {
        IncomingInstrument incomingInstrumentPRIME = new IncomingInstrument();
        incomingInstrumentPRIME.setCode("PB_03_2018");
        incomingInstrumentPRIME.setInstrumentType(InstrumentType.PRIME);
        incomingInstrumentPRIME.setLastTradingDate(ZonedDateTime.of(LocalDateTime.of(2018, 10, 15, 0, 0), ZoneId.of("Europe/London")));
        incomingInstrumentPRIME.setDeliveryDate(ZonedDateTime.of(LocalDateTime.of(2018, 10, 15, 0, 0), ZoneId.of("Europe/London")));
        incomingInstrumentPRIME.setMarket("LME_PB");
        incomingInstrumentPRIME.setLabel("Lead 13 March 2018");
        incomingInstrumentPRIME.setExchangeCode("PB_03_2018");
        incomingInstrumentPRIME.setTradable(false);
        return incomingInstrumentPRIME;
    }
}
