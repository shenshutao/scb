package scb.db;

import org.junit.Test;
import scb.model.InternalInstrument;
import scb.util.BasicTestComponents;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by shutao on 15/1/18.
 */
public class InternalInstrumentDatabaseTest extends BasicTestComponents {
    InternalInstrumentDatabase internalInstrumentDatabase = new InternalInstrumentDatabase();

    @Test
    public void testSaveAndGet() {
        InternalInstrument internalInstrument = getInternalInstrument();
        internalInstrumentDatabase.saveOrUpdateInternalInstrument("PB_03_2018", internalInstrument);

        InternalInstrument retrivedInternalInstrument = internalInstrumentDatabase.getInternalInstrument("PB_03_2018");
        assertThat(internalInstrument, equalTo(retrivedInternalInstrument));
    }
}
