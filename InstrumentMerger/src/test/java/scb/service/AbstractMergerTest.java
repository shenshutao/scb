package scb.service;

import org.junit.Test;
import scb.model.IncomingInstrument;
import scb.model.InternalInstrument;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by shutao on 14/1/18.
 */
public class AbstractMergerTest {
    @Test
    public void testGetTargetMarket() {
        MergerForTest merger = new MergerForTest();

        String result1 = merger.getTargetMarket("LME_PB");
        assertThat(result1, equalTo("PB"));

        String result2 = merger.getTargetMarket("PB");
        assertThat(result2, equalTo("PB"));
    }

    class MergerForTest extends AbstractMerger {
        @Override
        public InternalInstrument mergeInstrument(InternalInstrument internal, IncomingInstrument incoming) {
            // Do nothing.
            return null;
        }
    }
}
