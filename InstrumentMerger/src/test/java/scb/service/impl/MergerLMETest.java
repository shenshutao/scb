package scb.service.impl;

import org.junit.Test;
import scb.model.IncomingInstrument;
import scb.model.InternalInstrument;
import scb.service.AbstractMerger;
import scb.util.BasicTestComponents;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by shutao on 14/1/18.
 */
public class MergerLMETest extends BasicTestComponents {

    @Test
    public void testInternalInstrumentIsnull() {
        IncomingInstrument incomingInstrument = getIncomingInstrumentLME();

        AbstractMerger merger = new MergerLME();
        InternalInstrument result1 = merger.mergeInstrument(null, incomingInstrument);
        assertThat(result1.getDeliveryDate(), equalTo(ZonedDateTime.of(LocalDateTime.of(2018, 5, 15, 0, 0), ZoneId.of("Europe/London"))));
        assertThat(result1.getLastTradingDate(), equalTo(ZonedDateTime.of(LocalDateTime.of(2018, 5, 15, 0, 0), ZoneId.of("Europe/London"))));
        assertThat(result1.getMarket(), equalTo("PB"));
        assertThat(result1.getLabel(), equalTo("Lead 13 March 2018"));
        assertThat(result1.isTradable(), equalTo(true));
    }

    @Test
    public void testUpdateInstrument() {
        InternalInstrument internalInstrument = getInternalInstrument();
        IncomingInstrument incomingInstrument = getIncomingInstrumentLME();

        AbstractMerger merger = new MergerLME();
        InternalInstrument result1 = merger.mergeInstrument(internalInstrument, incomingInstrument);
        assertThat(result1.getDeliveryDate(), equalTo(ZonedDateTime.of(LocalDateTime.of(2018, 5, 15, 0, 0), ZoneId.of("Europe/London"))));
        assertThat(result1.getLastTradingDate(), equalTo(ZonedDateTime.of(LocalDateTime.of(2018, 5, 15, 0, 0), ZoneId.of("Europe/London"))));
        assertThat(result1.getMarket(), equalTo("PB"));
        assertThat(result1.getLabel(), equalTo("Lead 13 March 2018"));
        assertThat(result1.isTradable(), equalTo(true));
    }
}
