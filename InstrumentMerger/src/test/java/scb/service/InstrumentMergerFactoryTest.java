package scb.service;

import org.junit.Test;
import scb.model.InstrumentType;
import scb.service.impl.MergerLME;
import scb.service.impl.MergerPRIME;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;

/**
 * Created by shutao on 15/1/18.
 */
public class InstrumentMergerFactoryTest {
    @Test
    public void getLMEMerger() {
        AbstractMerger merger = InstrumentMergerFactory.getMerger(InstrumentType.LME);
        assertThat(merger, instanceOf(MergerLME.class));
    }

    @Test
    public void getPRIMEMerger() {
        AbstractMerger merger = InstrumentMergerFactory.getMerger(InstrumentType.PRIME);
        assertThat(merger, instanceOf(MergerPRIME.class));
    }
}
